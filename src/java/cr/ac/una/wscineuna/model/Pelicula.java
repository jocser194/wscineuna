/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_PELICULAS", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pelicula.findAll", query = "SELECT p FROM Pelicula p")
    , @NamedQuery(name = "Pelicula.findByPeId", query = "SELECT p FROM Pelicula p WHERE p.peId = :peId", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Pelicula.findByPeNombreEs", query = "SELECT p FROM Pelicula p WHERE p.peNombreEs = :peNombreEs")
    , @NamedQuery(name = "Pelicula.findByPeNombreIn", query = "SELECT p FROM Pelicula p WHERE p.peNombreIn = :peNombreIn")
    , @NamedQuery(name = "Pelicula.findByPeIdioma", query = "SELECT p FROM Pelicula p WHERE p.peIdioma = :peIdioma")
    , @NamedQuery(name = "Pelicula.findByPeResennaEs", query = "SELECT p FROM Pelicula p WHERE p.peResennaEs = :peResennaEs")
    , @NamedQuery(name = "Pelicula.findByPeResennaIn", query = "SELECT p FROM Pelicula p WHERE p.peResennaIn = :peResennaIn")
    , @NamedQuery(name = "Pelicula.findByPeLinkEs", query = "SELECT p FROM Pelicula p WHERE p.peLinkEs = :peLinkEs")
    , @NamedQuery(name = "Pelicula.findByPeLinkIn", query = "SELECT p FROM Pelicula p WHERE p.peLinkIn = :peLinkIn")
    , @NamedQuery(name = "Pelicula.findByPeFestreno", query = "SELECT p FROM Pelicula p WHERE p.peFestreno = :peFestreno")
    , @NamedQuery(name = "Pelicula.findByPeImagen", query = "SELECT p FROM Pelicula p WHERE p.peImagen = :peImagen")
    , @NamedQuery(name = "Pelicula.findByPeEstado", query = "SELECT p FROM Pelicula p WHERE p.peEstado = :peEstado")
    , @NamedQuery(name = "Pelicula.findByPeGenero", query = "SELECT p FROM Pelicula p WHERE p.peGenero = :peGenero")
    , @NamedQuery(name = "Pelicula.findByPeVersion", query = "SELECT p FROM Pelicula p WHERE p.peVersion = :peVersion")})
public class Pelicula implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_PELICULA_ID_GENERATOR", sequenceName = "cineuna.CI_PELICULA_SEQ05", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_PELICULA_ID_GENERATOR")

    @Basic(optional = false)
//    @NotNull
    @Column(name = "PE_ID")
    private Long peId;
//    @Size(max = 40)
    @Column(name = "PE_NOMBRE_ES")
    private String peNombreEs;
//    @Size(max = 40)
    @Column(name = "PE_NOMBRE_IN")
    private String peNombreIn;
//    @Size(max = 20)
    @Column(name = "PE_IDIOMA")
    private String peIdioma;
//    @Size(max = 700)
    @Column(name = "PE_RESENNA_ES")
    private String peResennaEs;
//    @Size(max = 700)
    @Column(name = "PE_RESENNA_IN")
    private String peResennaIn;
//    @Size(max = 300)
    @Column(name = "PE_LINK_ES")
    private String peLinkEs;
//    @Size(max = 300)
    @Column(name = "PE_LINK_IN")
    private String peLinkIn;
    @Basic(optional = false)
//    @NotNull
    @Column(name = "PE_FESTRENO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date peFestreno;
//    @Size(max = 200)
    @Column(name = "PE_IMAGEN")
    private String peImagen;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "PE_ESTADO")
    private String peEstado;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 300)
    @Column(name = "PE_GENERO")
    private String peGenero;
    @Version
//    @NotNull
    @Column(name = "PE_VERSION")
    private Long peVersion;
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "taIdPefk", fetch = FetchType.LAZY)
    private List<Tanda> tandas = new ArrayList();;

    public Pelicula() {
    }

    public Pelicula(Long peId) {
        this.peId = peId;
    }

    public Pelicula(Long peId, Date peFestreno, String peEstado, String peGenero) {
        this.peId = peId;
        this.peFestreno = peFestreno;
        this.peEstado = peEstado;
        this.peGenero = peGenero;
    }

    public Pelicula(PeliculaDto pelicula) {
        this.peId = pelicula.getPeId();
        actualizar(pelicula);
    }

    public void actualizar(PeliculaDto pelicula) {
        this.peEstado = pelicula.getPeEstado();
        this.peFestreno = Date.from(pelicula.getPeFestreno().atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.peIdioma = pelicula.getPeIdioma();
        this.peLinkEs = pelicula.getPeLinkEs();
        this.peLinkIn = pelicula.getPeLinkIn();
        this.peNombreEs = pelicula.getPeNombreEs();
        this.peNombreIn = pelicula.getPeNombreIn();
        this.peResennaEs = pelicula.getPeResennaEs();
        this.peResennaIn = pelicula.getPeResennaIn();
        this.peGenero = pelicula.getPeGenero();
    }

    public Long getPeId() {
        return peId;
    }

    public void setPeId(Long peId) {
        this.peId = peId;
    }

    public String getPeNombreEs() {
        return peNombreEs;
    }

    public void setPeNombreEs(String peNombreEs) {
        this.peNombreEs = peNombreEs;
    }

    public String getPeNombreIn() {
        return peNombreIn;
    }

    public void setPeNombreIn(String peNombreIn) {
        this.peNombreIn = peNombreIn;
    }

    public String getPeIdioma() {
        return peIdioma;
    }

    public void setPeIdioma(String peIdioma) {
        this.peIdioma = peIdioma;
    }

    public String getPeResennaEs() {
        return peResennaEs;
    }

    public void setPeResennaEs(String peResennaEs) {
        this.peResennaEs = peResennaEs;
    }

    public String getPeResennaIn() {
        return peResennaIn;
    }

    public void setPeResennaIn(String peResennaIn) {
        this.peResennaIn = peResennaIn;
    }

    public String getPeLinkEs() {
        return peLinkEs;
    }

    public void setPeLinkEs(String peLinkEs) {
        this.peLinkEs = peLinkEs;
    }

    public String getPeLinkIn() {
        return peLinkIn;
    }

    public void setPeLinkIn(String peLinkIn) {
        this.peLinkIn = peLinkIn;
    }

    public Date getPeFestreno() {
        return peFestreno;
    }

    public void setPeFestreno(Date peFestreno) {
        this.peFestreno = peFestreno;
    }

    public String getPeImagen() {
        return peImagen;
    }

    public void setPeImagen(String peImagen) {
        this.peImagen = peImagen;
    }

    public String getPeEstado() {
        return peEstado;
    }

    public void setPeEstado(String peEstado) {
        this.peEstado = peEstado;
    }

    public String getPeGenero() {
        return peGenero;
    }

    public void setPeGenero(String peGenero) {
        this.peGenero = peGenero;
    }

  

    @XmlTransient
    public List<Tanda> getTandas() {
        return tandas;
    }

    public void setTandas(List<Tanda> tandaList) {
        this.tandas = tandaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (peId != null ? peId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pelicula)) {
            return false;
        }
        Pelicula other = (Pelicula) object;
        if ((this.peId == null && other.peId != null) || (this.peId != null && !this.peId.equals(other.peId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Pelicula[ peId=" + peId + " ]";
    }

}
