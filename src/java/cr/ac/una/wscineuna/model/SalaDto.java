/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "SalaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SalaDto {
    private Long saId; 
    private String saNombre;
    private String saEstado;
    private String saFonImagen;
    private List<TandaDto> tandas = new ArrayList();
    private List<AsientoDto> asientos = new ArrayList();
    private List<AsientoDto> elimados = new ArrayList();
    

    public SalaDto() {
    }

    public SalaDto(Sala sala) {
        this.saId = sala.getSaId();
        this.saNombre = sala.getSaNombre();
        this.saEstado = sala.getSaEstado();
        this.saNombre =  sala.getSaNombre();
    }

    public Long getSaId() {
        return saId;
    }

    public void setSaId(Long saId) {
        this.saId = saId;
    }

    public String getSaNombre() {
        return saNombre;
    }

    public void setSaNombre(String saNombre) {
        this.saNombre = saNombre;
    }

    public String getSaEstado() {
        return saEstado;
    }

    public void setSaEstado(String saEstado) {
        this.saEstado = saEstado;
    }

    public List<TandaDto> getTandas() {
        return tandas;
    }

    public void setTandas(List<TandaDto> tandaList) {
        this.tandas = tandaList;
    }

    public List<AsientoDto> getAsientos() {
        return asientos;
    }

    public void setAsientos(List<AsientoDto> asientoList) {
        this.asientos = asientoList;
    }

    public List<AsientoDto> getElimados() {
        return elimados;
    }

    public void setElimados(List<AsientoDto> elimados) {
        this.elimados = elimados;
    }

    public String getSaFonImagen() {
        return saFonImagen;
    }

    public void setSaFonImagen(String saFonImagen) {
        this.saFonImagen = saFonImagen;
    }

    @Override
    public String toString() {
        return "SalaDto{" + "saId=" + saId + ", saNombre=" + saNombre + ", saEstado=" + saEstado + ", tandas=" + tandas + ", asientos=" + asientos + ", elimados=" + elimados + '}';
    }

    
    
}
