/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "TandaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class TandaDto {

    private Long taId;
    private String taIdioma;
    private Integer taCosto;
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private String taFhinicio;
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private String taFhfin;
    private List<CompraDto> compras = new ArrayList();
    private PeliculaDto taIdPefk;
    private SalaDto taIdSafk;

    public TandaDto() {
    }

    public TandaDto(Tanda tanda) {
        this.taId = tanda.getTaId();
        this.taIdioma = tanda.getTaIdioma();
        this.taCosto = tanda.getTaCosto();
        this.taFhinicio = tanda.getTaFhinicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString();
        this.taFhfin = tanda.getTaFhfin().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString();
    }

    public Long getTaId() {
        return taId;
    }

    public void setTaId(Long taId) {
        this.taId = taId;
    }

    public String getTaIdioma() {
        return taIdioma;
    }

    public void setTaIdioma(String taIdioma) {
        this.taIdioma = taIdioma;
    }

    public Integer getTaCosto() {
        return taCosto;
    }

    public void setTaCosto(Integer taCosto) {
        this.taCosto = taCosto;
    }

    public String getTaFhinicio() {
        return taFhinicio;
    }

    public void setTaFhinicio(String taFhinicio) {
        this.taFhinicio = taFhinicio;
    }

    public String getTaFhfin() {
        return taFhfin;
    }

    public void setTaFhfin(String taFhfin) {
        this.taFhfin = taFhfin;
    }

    public List<CompraDto> getCompras() {
        return compras;
    }

    public void setCompras(List<CompraDto> compras) {
        this.compras = compras;
    }


    public PeliculaDto getTaIdPefk() {
        return taIdPefk;
    }

    public void setTaIdPefk(PeliculaDto taIdPefk) {
        this.taIdPefk = taIdPefk;
    }

    public SalaDto getTaIdSafk() {
        return taIdSafk;
    }

    public void setTaIdSafk(SalaDto taIdSafk) {
        this.taIdSafk = taIdSafk;
    }

    @Override
    public String toString() {
        return "TandaDto{" + "taId=" + taId + ", taIdioma=" + taIdioma + ", taCosto=" + taCosto + ", taFhinicio=" + taFhinicio + ", taFhfin=" + taFhfin + ", compras=" + compras + ", taIdPefk=" + taIdPefk + ", taIdSafk=" + taIdSafk + '}';
    }

    
    
}
