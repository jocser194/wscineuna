/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "CineDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CineDto {
    private Long cid;
    private String cNombre;
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private String cHorai;
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private String cHoraf;
    private String correo;
    private Integer cTelefono;

    public CineDto() {
    }

    public CineDto(Cine cine) {
        this.cid = cine.getCId();
        this.cNombre = cine.getCNombre();
        this.cHorai = cine.getCHorai().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString();
        this.cHoraf = cine.getCHoraf().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString();
        this.correo = cine.getCCorreo();
        this.cTelefono = cine.getCTelefono();
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public String getcHorai() {
        return cHorai;
    }

    public void setcHorai(String cHorai) {
        this.cHorai = cHorai;
    }

    public String getcHoraf() {
        return cHoraf;
    }

    public void setcHoraf(String cHoraf) {
        this.cHoraf = cHoraf;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getcTelefono() {
        return cTelefono;
    }

    public void setcTelefono(Integer cTelefono) {
        this.cTelefono = cTelefono;
    }

    @Override
    public String toString() {
        return "CineDto{" + "cid=" + cid + ", cNombre=" + cNombre + ", cHorai=" + cHorai + ", cHoraf=" + cHoraf + ", correo=" + correo + ", cTelefono=" + cTelefono + '}';
    }
    
}
