/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_SALA", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sala.findAll", query = "SELECT s FROM Sala s")
    , @NamedQuery(name = "Sala.findBySaId", query = "SELECT s FROM Sala s WHERE s.saId = :saId")
    , @NamedQuery(name = "Sala.findBySaNombre", query = "SELECT s FROM Sala s WHERE s.saNombre = :saNombre")
    , @NamedQuery(name = "Sala.findBySaEstado", query = "SELECT s FROM Sala s WHERE s.saEstado = :saEstado")
    , @NamedQuery(name = "Sala.findBySaFonImagen", query = "SELECT s FROM Sala s WHERE s.saFonImagen = :saFonImagen")
    , @NamedQuery(name = "Sala.findBySaVersion", query = "SELECT s FROM Sala s WHERE s.saVersion = :saVersion")})
public class Sala implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_SALA_ID_GENERATOR", sequenceName = "cineuna.CI_SALA_SEQ02", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_SALA_ID_GENERATOR")

    @Basic(optional = false)
//    @NotNull
    @Column(name = "SA_ID")
    private Long saId;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 30)
    @Column(name = "SA_NOMBRE")
    private String saNombre;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "SA_ESTADO")
    private String saEstado;
//    @Size(max = 200)
    @Column(name = "SA_FON_IMAGEN")
    private String saFonImagen;
    @Version
    @Column(name = "SA_VERSION")
    private Long saVersion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taIdSafk", fetch = FetchType.LAZY)
    private List<Tanda> tandas = new ArrayList();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asIdSala", fetch = FetchType.LAZY)
    private List<Asiento> asientos = new ArrayList();

    public Sala() {
    }

    public Sala(Long saId) {
        this.saId = saId;
    }

    public Sala(Long saId, String saNombre, String saEstado) {
        this.saId = saId;
        this.saNombre = saNombre;
        this.saEstado = saEstado;
    }

    public Sala(SalaDto sala) {
        this.saId = sala.getSaId();
        actualizar(sala);
    }

    public void actualizar(SalaDto sala) {
        this.saEstado = sala.getSaEstado();
        this.saNombre = sala.getSaNombre();
    }

    public Long getSaId() {
        return saId;
    }

    public void setSaId(Long saId) {
        this.saId = saId;
    }

    public String getSaNombre() {
        return saNombre;
    }

    public void setSaNombre(String saNombre) {
        this.saNombre = saNombre;
    }

    public String getSaEstado() {
        return saEstado;
    }

    public void setSaEstado(String saEstado) {
        this.saEstado = saEstado;
    }

    public String getSaFonImagen() {
        return saFonImagen;
    }

    public void setSaFonImagen(String saFonImagen) {
        this.saFonImagen = saFonImagen;
    }

    @XmlTransient
    public List<Tanda> getTandas() {
        return tandas;
    }

    public void setTandaList(List<Tanda> tandaList) {
        this.tandas = tandaList;
    }

    @XmlTransient
    public List<Asiento> getAsientos() {
        return asientos;
    }

    public void setAsientos(List<Asiento> asientoList) {
        this.asientos = asientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saId != null ? saId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sala)) {
            return false;
        }
        Sala other = (Sala) object;
        if ((this.saId == null && other.saId != null) || (this.saId != null && !this.saId.equals(other.saId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Sala[ saId=" + saId + " ]";
    }

}
