/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import cr.ac.una.wscineuna.util.LocalDateAdapter;
import cr.ac.una.wscineuna.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "ReporteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReporteDto {
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fecha1;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fecha2;
    private String arCode;

    public ReporteDto(String arCode) {
        this.arCode =  arCode;
        fecha1 = LocalDate.now();
        fecha2 = LocalDate.now();
    }

    public ReporteDto() {
    }

    public LocalDate getFecha1() {
        return fecha1;
    }

    public void setFecha1(LocalDate fecha1) {
        this.fecha1 = fecha1;
    }

    public LocalDate getFecha2() {
        return fecha2;
    }

    public void setFecha2(LocalDate fecha2) {
        this.fecha2 = fecha2;
    }

    public String getArCode() {
        return arCode;
    }

    public void setArCode(String arCode) {
        this.arCode = arCode;
    }
    
}
