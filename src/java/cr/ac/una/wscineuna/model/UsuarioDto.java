/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "UsuarioDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class UsuarioDto {

    private Long usId;
    private String usNombre;
    private String usApellido;
    private String usCedula;
    private Integer usTelefono;
    private String usUsuario;
    private String usClave;
    private String usCorreo;
    private String usIdioma;
    private String usActivo;
    private String usGenero;
    private String usAdministrador;
    private String usRecuperarClave;
    private String usCodigoActivacion;

    public UsuarioDto() {
    }

    public UsuarioDto(Usuario usuario) {
        this.usId = usuario.getUsId();
        this.usNombre = usuario.getUsNombre();
        this.usApellido = usuario.getUsApellido();
        this.usCedula = usuario.getUsCedula();
        this.usTelefono = usuario.getUsTelefono();
        this.usUsuario = usuario.getUsUsuario();
        this.usClave = usuario.getUsClave();
        this.usCorreo = usuario.getUsCorreo();
        this.usIdioma = usuario.getUsIdioma();
        this.usActivo = usuario.getUsActivo();
        this.usGenero = usuario.getUsGenero();
        this.usAdministrador = usuario.getUsAdministrador();
        this.usCodigoActivacion = usuario.getUaCodigoActivacion();
        this.usRecuperarClave = usuario.getUsRecuperacionclave();
    }

    public Long getUsId() {
        return usId;
    }

    public void setUsId(Long usId) {
        this.usId = usId;
    }

    public String getUsNombre() {
        return usNombre;
    }

    public void setUsNombre(String usNombre) {
        this.usNombre = usNombre;
    }

    public String getUsApellido() {
        return usApellido;
    }

    public void setUsApellido(String usApellido) {
        this.usApellido = usApellido;
    }

    public String getUsCedula() {
        return usCedula;
    }

    public void setUsCedula(String usCedula) {
        this.usCedula = usCedula;
    }

    public Integer getUsTelefono() {
        return usTelefono;
    }

    public void setUsTelefono(Integer usTelefono) {
        this.usTelefono = usTelefono;
    }

    public String getUsUsuario() {
        return usUsuario;
    }

    public void setUsUsuario(String usUsuario) {
        this.usUsuario = usUsuario;
    }

    public String getUsClave() {
        return usClave;
    }

    public void setUsClave(String usClave) {
        this.usClave = usClave;
    }

    public String getUsGenero() {
        return usGenero;
    }

    public void setUsGenero(String usGenero) {
        this.usGenero = usGenero;
    }

    public String getUsCorreo() {
        return usCorreo;
    }

    public void setUsCorreo(String usCorreo) {
        this.usCorreo = usCorreo;
    }

    public String getUsIdioma() {
        return usIdioma;
    }

    public void setUsIdioma(String usIdioma) {
        this.usIdioma = usIdioma;
    }

    public String getUsActivo() {
        return usActivo;
    }

    public void setUsActivo(String usActivo) {
        this.usActivo = usActivo;
    }

    public String getUsAdministrador() {
        return usAdministrador;
    }

    public void setUsAdministrador(String usAdministrador) {
        this.usAdministrador = usAdministrador;
    }

    public String getUsRecuperarClave() {
        return usRecuperarClave;
    }

    public void setUsRecuperarClave(String usRecuperarClave) {
        this.usRecuperarClave = usRecuperarClave;
    }

    public String getUsCodigoActivacion() {
        return usCodigoActivacion;
    }

    public void setUsCodigoActivacion(String usCodigoActivacion) {
        this.usCodigoActivacion = usCodigoActivacion;
    }

    @Override
    public String toString() {
        return "UsuarioDto{" + "usId=" + usId + ", usNombre=" + usNombre + ", usApellido=" + usApellido + ", usCedula=" + usCedula + ", usTelefono=" + usTelefono + ", usUsuario=" + usUsuario + ", usClave=" + usClave + ", usCorreo=" + usCorreo + ", usIdioma=" + usIdioma + ", usActivo=" + usActivo + ", usAdministrador=" + usAdministrador + '}';
    }

}
