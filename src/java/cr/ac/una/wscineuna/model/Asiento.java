/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_ASIENTO",schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asiento.findAll", query = "SELECT a FROM Asiento a")
    , @NamedQuery(name = "Asiento.findByAsId", query = "SELECT a FROM Asiento a WHERE a.asId = :asId")
    , @NamedQuery(name = "Asiento.findByAsFila", query = "SELECT a FROM Asiento a WHERE a.asFila = :asFila")
    , @NamedQuery(name = "Asiento.findByAsNumAsiento", query = "SELECT a FROM Asiento a WHERE a.asNumAsiento = :asNumAsiento")
    , @NamedQuery(name = "Asiento.findByAsEstado", query = "SELECT a FROM Asiento a WHERE a.asEstado = :asEstado")
    , @NamedQuery(name = "Asiento.findByAsNumTipoimagen", query = "SELECT a FROM Asiento a WHERE a.asNumTipoimagen = :asNumTipoimagen")
    , @NamedQuery(name = "Asiento.findByAsVersion", query = "SELECT a FROM Asiento a WHERE a.asVersion = :asVersion")})
public class Asiento implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_ASIENTO_ID_GENERATOR", sequenceName = "cineuna.CI_ASIENTO_SEQ04", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_ASIENTO_ID_GENERATOR")

    @Basic(optional = false)
    @NotNull
    @Column(name = "AS_ID")
    private Long asId;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "AS_FILA")
    private String asFila;
    @Basic(optional = false)
//    @NotNull
    @Column(name = "AS_NUM_ASIENTO")
    private Integer asNumAsiento;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "AS_ESTADO")
    private String asEstado;
    @Column(name = "AS_NUM_TIPOIMAGEN")
    private Integer asNumTipoimagen;
    @Version
    @Column(name = "AS_VERSION")
    private Long asVersion;
    @OneToMany(cascade = CascadeType.MERGE,mappedBy = "boAsid", fetch = FetchType.LAZY)
    private List<Boleto> boletos;
    @JoinColumn(name = "AS_ID_SALA", referencedColumnName = "SA_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Sala asIdSala;

    public Asiento() {
    }

    public Asiento(Long asId) {
        this.asId = asId;
    }

    public Asiento(Long asId, String asFila, Integer asNumAsiento, String asEstado) {
        this.asId = asId;
        this.asFila = asFila;
        this.asNumAsiento = asNumAsiento;
        this.asEstado = asEstado;
    }

    public Asiento(AsientoDto asiento) {
        this.asId = asiento.getAsId();
        actualizr(asiento);
    }

    public void actualizr(AsientoDto asiento) {
        this.asEstado = asiento.getAsEstado();
        this.asFila = asiento.getAsFila();
        this.asNumAsiento = asiento.getAsNumAsiento();
        this.asNumTipoimagen = asiento.getAsNumTipoimagen();
    }

    public Long getAsId() {
        return asId;
    }

    public void setAsId(Long asId) {
        this.asId = asId;
    }

    public String getAsFila() {
        return asFila;
    }

    public void setAsFila(String asFila) {
        this.asFila = asFila;
    }

    public Integer getAsNumAsiento() {
        return asNumAsiento;
    }

    public void setAsNumAsiento(Integer asNumAsiento) {
        this.asNumAsiento = asNumAsiento;
    }

    public String getAsEstado() {
        return asEstado;
    }

    public void setAsEstado(String asEstado) {
        this.asEstado = asEstado;
    }

    public Integer getAsNumTipoimagen() {
        return asNumTipoimagen;
    }

    public void setAsNumTipoimagen(Integer asNumTipoimagen) {
        this.asNumTipoimagen = asNumTipoimagen;
    }

  

    @XmlTransient
    public List<Boleto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<Boleto> boletoList) {
        this.boletos = boletoList;
    }

    public Sala getAsIdSala() {
        return asIdSala;
    }

    public void setAsIdSala(Sala asIdSala) {
        this.asIdSala = asIdSala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asId != null ? asId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asiento)) {
            return false;
        }
        Asiento other = (Asiento) object;
        if ((this.asId == null && other.asId != null) || (this.asId != null && !this.asId.equals(other.asId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Asiento[ asId=" + asId + " ]";
    }

}
