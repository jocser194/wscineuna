/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import cr.ac.una.wscineuna.util.LocalDateAdapter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
public class CompraDto {

    private Long coId;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate coFechacompra;
    private TandaDto coTaId;
    private List<BoletoDto> boletos;
    private UsuarioDto coUsid;

    public CompraDto() {
    }

    public CompraDto(Compra con) {
        this.coId = con.getCoId();
        this.coFechacompra = con.getCoFechacompra().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public Long getCoId() {
        return coId;
    }

    public void setCoId(Long coId) {
        this.coId = coId;
    }

    public LocalDate getCoFechacompra() {
        return coFechacompra;
    }

    public void setCoFechacompra(LocalDate coFechacompra) {
        this.coFechacompra = coFechacompra;
    }

    public List<BoletoDto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<BoletoDto> boletos) {
        this.boletos = boletos;
    }

    public TandaDto getCoTaId() {
        return coTaId;
    }

    public void setCoTaId(TandaDto coTaId) {
        this.coTaId = coTaId;
    }

    public UsuarioDto getCoUsid() {
        return coUsid;
    }

    public void setCoUsid(UsuarioDto coUsid) {
        this.coUsid = coUsid;
    }

}
