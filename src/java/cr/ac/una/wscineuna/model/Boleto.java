/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_BOLETO", schema ="cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Boleto.findAll", query = "SELECT b FROM Boleto b")
    , @NamedQuery(name = "Boleto.findByBoId", query = "SELECT b FROM Boleto b WHERE b.boId = :boId")
    , @NamedQuery(name = "Boleto.findByBoEstado", query = "SELECT b FROM Boleto b WHERE b.boEstado = :boEstado")
    , @NamedQuery(name = "Boleto.findByBoFilaAsiento", query = "SELECT b FROM Boleto b WHERE b.boFilaAsiento = :boFilaAsiento")
    , @NamedQuery(name = "Boleto.findByBoNumeroAsiento", query = "SELECT b FROM Boleto b WHERE b.boNumeroAsiento = :boNumeroAsiento")
    , @NamedQuery(name = "Boleto.findByBoVersion", query = "SELECT b FROM Boleto b WHERE b.boVersion = :boVersion")})
public class Boleto implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_BOLETO_ID_GENERATOR", sequenceName = "cineuna.CI_BOLETO_SEQ06", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_BOLETO_ID_GENERATOR")
    @Basic(optional = false)
//    @NotNull
    @Column(name = "BO_ID")
    private Long boId;
//    @Size(max = 1)
    @Column(name = "BO_ESTADO")
    private String boEstado;
//    @Size(max = 1)
    @Column(name = "BO_FILA_ASIENTO")
    private String boFilaAsiento;
    @Column(name = "BO_NUMERO_ASIENTO")
    private Integer boNumeroAsiento;
    @Version
//    @NotNull
    @Column(name = "BO_VERSION")
    private Long boVersion;
    @JoinColumn(name = "BO_ASID", referencedColumnName = "AS_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Asiento boAsid;
    @JoinColumn(name = "BO_COID", referencedColumnName = "CO_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Compra boCoid;

    public Boleto() {
    }

  
    public Boleto(Long boId) {
        this.boId = boId;
    }

     public Boleto(BoletoDto boleto) {
        this.boId = boleto.getBoId();
        actualizar(boleto);
    }

    public void actualizar(BoletoDto boleto) {
        this.boEstado = boleto.getBoEstado();
        this.boFilaAsiento = boleto.getBoFilaAsiento();
        this.boNumeroAsiento = boleto.getBoNumeroAsiento();
    }

    public Long getBoId() {
        return boId;
    }

    public void setBoId(Long boId) {
        this.boId = boId;
    }

    public String getBoEstado() {
        return boEstado;
    }

    public void setBoEstado(String boEstado) {
        this.boEstado = boEstado;
    }

    public String getBoFilaAsiento() {
        return boFilaAsiento;
    }

    public void setBoFilaAsiento(String boFilaAsiento) {
        this.boFilaAsiento = boFilaAsiento;
    }

    public Integer getBoNumeroAsiento() {
        return boNumeroAsiento;
    }

    public void setBoNumeroAsiento(Integer boNumeroAsiento) {
        this.boNumeroAsiento = boNumeroAsiento;
    }


    public Asiento getBoAsid() {
        return boAsid;
    }

    public void setBoAsid(Asiento boAsid) {
        this.boAsid = boAsid;
    }

    public Compra getBoCoid() {
        return boCoid;
    }

    public void setBoCoid(Compra boCoid) {
        this.boCoid = boCoid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (boId != null ? boId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boleto)) {
            return false;
        }
        Boleto other = (Boleto) object;
        if ((this.boId == null && other.boId != null) || (this.boId != null && !this.boId.equals(other.boId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Boleto[ boId=" + boId + " ]";
    }

}
