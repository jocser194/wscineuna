/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_COMPRA", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c")
    , @NamedQuery(name = "Compra.findByCoId", query = "SELECT c FROM Compra c WHERE c.coId = :coId")
    , @NamedQuery(name = "Compra.findByCoFechacompra", query = "SELECT c FROM Compra c WHERE c.coFechacompra = :coFechacompra")
    , @NamedQuery(name = "Compra.findByCoVersion", query = "SELECT c FROM Compra c WHERE c.coVersion = :coVersion")})
public class Compra implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_Compra_ID_GENERATOR", sequenceName = "cineuna.CI_COMPRA_SEQ08", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_Compra_ID_GENERATOR")
    @Basic(optional = false)
//    @NotNull
    @Column(name = "CO_ID")
    private Long coId;
    @Column(name = "CO_FECHACOMPRA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date coFechacompra;
    @Version
    @Column(name = "CO_VERSION")
    private Long coVersion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "boCoid", fetch = FetchType.LAZY)
    private List<Boleto> boletos;
    @JoinColumn(name = "CO_TA_ID", referencedColumnName = "TA_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Tanda coTaId;
    @JoinColumn(name = "CO_USID", referencedColumnName = "US_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Usuario coUsid;

    public Compra() {
    }

    public Compra(CompraDto con) {
        this.coId = con.getCoId();
        actualizar(con);
    }
    public void actualizar(CompraDto con){
         this.coFechacompra = Date.from(con.getCoFechacompra().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    public Compra(Long coId) {
        this.coId = coId;
    }

    public Long getCoId() {
        return coId;
    }

    public void setCoId(Long coId) {
        this.coId = coId;
    }

    public Date getCoFechacompra() {
        return coFechacompra;
    }

    public void setCoFechacompra(Date coFechacompra) {
        this.coFechacompra = coFechacompra;
    }

    @XmlTransient
    public List<Boleto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<Boleto> boletoList) {
        this.boletos = boletoList;
    }

    public Tanda getCoTaId() {
        return coTaId;
    }

    public void setCoTaId(Tanda coTaId) {
        this.coTaId = coTaId;
    }

    public Usuario getCoUsid() {
        return coUsid;
    }

    public void setCoUsid(Usuario coUsid) {
        this.coUsid = coUsid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coId != null ? coId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.coId == null && other.coId != null) || (this.coId != null && !this.coId.equals(other.coId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Compra[ coId=" + coId + " ]";
    }

}
