/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_USUARIOS", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsId", query = "SELECT u FROM Usuario u WHERE u.usId = :usId")
    , @NamedQuery(name = "Usuario.findByUsNombre", query = "SELECT u FROM Usuario u WHERE u.usNombre = :usNombre")
    , @NamedQuery(name = "Usuario.findByUsApellido", query = "SELECT u FROM Usuario u WHERE u.usApellido = :usApellido")
    , @NamedQuery(name = "Usuario.findByUsCedula", query = "SELECT u FROM Usuario u WHERE u.usCedula = :usCedula")
    , @NamedQuery(name = "Usuario.findByUsTelefono", query = "SELECT u FROM Usuario u WHERE u.usTelefono = :usTelefono")
    , @NamedQuery(name = "Usuario.findByUsUsuario", query = "SELECT u FROM Usuario u WHERE u.usUsuario = :usUsuario", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Usuario.findByUsClave", query = "SELECT u FROM Usuario u WHERE u.usClave = :usClave")
    , @NamedQuery(name = "Usuario.findByUsCorreo", query = "SELECT u FROM Usuario u WHERE u.usCorreo = :usCorreo")
    , @NamedQuery(name = "Usuario.findByUsIdioma", query = "SELECT u FROM Usuario u WHERE u.usIdioma = :usIdioma")
    , @NamedQuery(name = "Usuario.findByUsActivo", query = "SELECT u FROM Usuario u WHERE u.usActivo = :usActivo")
    , @NamedQuery(name = "Usuario.findByUsAdministrador", query = "SELECT u FROM Usuario u WHERE u.usAdministrador = :usAdministrador")
    , @NamedQuery(name = "Usuario.findByUsGenero", query = "SELECT u FROM Usuario u WHERE u.usGenero = :usGenero")
    , @NamedQuery(name = "Usuario.findByUsRecuperacionclave", query = "SELECT u FROM Usuario u WHERE u.usRecuperacionclave = :usRecuperacionclave")
    , @NamedQuery(name = "Usuario.findByUaCodigoActivacion", query = "SELECT u FROM Usuario u WHERE u.uaCodigoActivacion = :uaCodigoActivacion")
    , @NamedQuery(name = "Usuario.findByUsuClave", query = "SELECT u FROM Usuario u WHERE u.usUsuario = :usUsuario and u.usClave = :usClave", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Usuario.findByUsVersion", query = "SELECT u FROM Usuario u WHERE u.usVersion = :usVersion")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_USUARIO_ID_GENERATOR", sequenceName = "cineuna.CI_USUARIOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_USUARIO_ID_GENERATOR")
    @Basic(optional = false)
//    @NotNull
    @Column(name = "US_ID")
    private Long usId;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 40)
    @Column(name = "US_NOMBRE")
    private String usNombre;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 40)
    @Column(name = "US_APELLIDO")
    private String usApellido;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 40)
    @Column(name = "US_CEDULA")
    private String usCedula;
    @Column(name = "US_TELEFONO")
    private Integer usTelefono;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 40)
    @Column(name = "US_USUARIO")
    private String usUsuario;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 20)
    @Column(name = "US_CLAVE")
    private String usClave;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 40)
    @Column(name = "US_CORREO")
    private String usCorreo;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 2)
    @Column(name = "US_IDIOMA")
    private String usIdioma;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "US_ACTIVO")
    private String usActivo;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "US_ADMINISTRADOR")
    private String usAdministrador;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 1)
    @Column(name = "US_GENERO")
    private String usGenero;
//    @Size(max = 1)
    @Column(name = "US_RECUPERACIONCLAVE")
    private String usRecuperacionclave;
//    @Size(max = 30)
    @Column(name = "UA_CODIGO_ACTIVACION")
    private String uaCodigoActivacion;
    @Version
    @Column(name = "US_VERSION")
    private Long usVersion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "coUsid", fetch = FetchType.LAZY)
    private List<Compra> compras;

    public Usuario() {
    }

    public Usuario(Long usId) {
        this.usId = usId;
    }

    public Usuario(Long usId, String usNombre, String usApellido, String usCedula, String usUsuario, String usClave, String usCorreo, String usIdioma, String usActivo, String usAdministrador, String usGenero) {
        this.usId = usId;
        this.usNombre = usNombre;
        this.usApellido = usApellido;
        this.usCedula = usCedula;
        this.usUsuario = usUsuario;
        this.usClave = usClave;
        this.usCorreo = usCorreo;
        this.usIdioma = usIdioma;
        this.usActivo = usActivo;
        this.usAdministrador = usAdministrador;
        this.usGenero = usGenero;
    }

    public Usuario(UsuarioDto usuario) {
        this.usId = usuario.getUsId();
        actualizarUsCed(usuario);
        actualizar(usuario);
    }

    /**
     * solo en caso de querer atualizar la cedula o usuario
     *
     * @param usuario
     */
    public void actualizarUsCed(UsuarioDto usuario) {
        this.usUsuario = usuario.getUsUsuario();
        this.usCedula = usuario.getUsCedula();
    }

    public void actualizar(UsuarioDto usuario) {
        this.usActivo = usuario.getUsActivo();
        this.usAdministrador = usuario.getUsAdministrador();
        this.usApellido = usuario.getUsApellido();
        this.usClave = usuario.getUsClave();
        this.usCorreo = usuario.getUsCorreo();
        this.usIdioma = usuario.getUsIdioma();
        this.usNombre = usuario.getUsNombre();
        this.usGenero = usuario.getUsGenero();
        this.usTelefono = usuario.getUsTelefono();
        this.uaCodigoActivacion = usuario.getUsCodigoActivacion();
        this.usRecuperacionclave = usuario.getUsRecuperarClave();
    }

    public Long getUsId() {
        return usId;
    }

    public void setUsId(Long usId) {
        this.usId = usId;
    }

    public String getUsNombre() {
        return usNombre;
    }

    public void setUsNombre(String usNombre) {
        this.usNombre = usNombre;
    }

    public String getUsApellido() {
        return usApellido;
    }

    public void setUsApellido(String usApellido) {
        this.usApellido = usApellido;
    }

    public String getUsCedula() {
        return usCedula;
    }

    public void setUsCedula(String usCedula) {
        this.usCedula = usCedula;
    }

    public Integer getUsTelefono() {
        return usTelefono;
    }

    public void setUsTelefono(Integer usTelefono) {
        this.usTelefono = usTelefono;
    }

    public String getUsUsuario() {
        return usUsuario;
    }

    public void setUsUsuario(String usUsuario) {
        this.usUsuario = usUsuario;
    }

    public String getUsClave() {
        return usClave;
    }

    public void setUsClave(String usClave) {
        this.usClave = usClave;
    }

    public String getUsCorreo() {
        return usCorreo;
    }

    public void setUsCorreo(String usCorreo) {
        this.usCorreo = usCorreo;
    }

    public String getUsIdioma() {
        return usIdioma;
    }

    public void setUsIdioma(String usIdioma) {
        this.usIdioma = usIdioma;
    }

    public String getUsActivo() {
        return usActivo;
    }

    public void setUsActivo(String usActivo) {
        this.usActivo = usActivo;
    }

    public String getUsAdministrador() {
        return usAdministrador;
    }

    public void setUsAdministrador(String usAdministrador) {
        this.usAdministrador = usAdministrador;
    }

    public String getUsGenero() {
        return usGenero;
    }

    public void setUsGenero(String usGenero) {
        this.usGenero = usGenero;
    }

    public String getUsRecuperacionclave() {
        return usRecuperacionclave;
    }

    public void setUsRecuperacionclave(String usRecuperacionclave) {
        this.usRecuperacionclave = usRecuperacionclave;
    }

    public String getUaCodigoActivacion() {
        return uaCodigoActivacion;
    }

    public void setUaCodigoActivacion(String uaCodigoActivacion) {
        this.uaCodigoActivacion = uaCodigoActivacion;
    }


    @XmlTransient
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compraList) {
        this.compras = compraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usId != null ? usId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usId == null && other.usId != null) || (this.usId != null && !this.usId.equals(other.usId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Usuario[ usId=" + usId + " ]";
    }

}
