/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_CINE", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cine.findAll", query = "SELECT c FROM Cine c")
    , @NamedQuery(name = "Cine.findByCId", query = "SELECT c FROM Cine c WHERE c.cId = :cId")})
public class Cine implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_CINE_ID_GENERATOR", sequenceName = "cineuna.CI_CINE_SEQ07", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_CINE_ID_GENERATOR")

    @Basic(optional = false)
//    @NotNull
    @Column(name = "C_ID")
    private Long cId;
//    @Size(max = 40)
    @Column(name = "C_NOMBRE")
    private String cNombre;
    @Column(name = "C_HORAI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cHorai;
    @Column(name = "C_HORAF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cHoraf;
//    @Size(max = 40)
    @Column(name = "C_CORREO")
    private String cCorreo;
    @Column(name = "C_TELEFONO")
    private Integer cTelefono;
    @Version
//    @NotNull
    @Column(name = "C_VERSION")
    private Long cVersion;

    public Cine() {
    }

    public Cine(Long cId) {
        this.cId = cId;
    }

    public Cine(CineDto ci) {
        this.cId = ci.getCid();
        actualizar(ci);
    }

    public void actualizar(CineDto ci) {
        this.cNombre = ci.getcNombre();
        this.cHorai = Date.from(LocalDateTime.parse(ci.getcHorai()).atZone(ZoneId.systemDefault()).toInstant());
        this.cHoraf = Date.from(LocalDateTime.parse(ci.getcHoraf()).atZone(ZoneId.systemDefault()).toInstant());
        this.cCorreo = ci.getCorreo();
        this.cTelefono = ci.getcTelefono();
    }

    public Long getCId() {
        return cId;
    }

    public void setCId(Long cId) {
        this.cId = cId;
    }

    public String getCNombre() {
        return cNombre;
    }

    public void setCNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public Date getCHorai() {
        return cHorai;
    }

    public void setCHorai(Date cHorai) {
        this.cHorai = cHorai;
    }

    public Date getCHoraf() {
        return cHoraf;
    }

    public void setCHoraf(Date cHoraf) {
        this.cHoraf = cHoraf;
    }

    public String getCCorreo() {
        return cCorreo;
    }

    public void setCCorreo(String cCorreo) {
        this.cCorreo = cCorreo;
    }

    public Integer getCTelefono() {
        return cTelefono;
    }

    public void setCTelefono(Integer cTelefono) {
        this.cTelefono = cTelefono;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cId != null ? cId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cine)) {
            return false;
        }
        Cine other = (Cine) object;
        if ((this.cId == null && other.cId != null) || (this.cId != null && !this.cId.equals(other.cId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Cine[ cId=" + cId + " ]";
    }

}
