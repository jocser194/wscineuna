/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@Entity
@Table(name = "CI_TANDA", schema = "cineuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tanda.findAll", query = "SELECT t FROM Tanda t")
    , @NamedQuery(name = "Tanda.findByTaId", query = "SELECT t FROM Tanda t WHERE t.taId = :taId")
    , @NamedQuery(name = "Tanda.findByTaIdioma", query = "SELECT t FROM Tanda t WHERE t.taIdioma = :taIdioma")
    , @NamedQuery(name = "Tanda.findByTaCosto", query = "SELECT t FROM Tanda t WHERE t.taCosto = :taCosto")
    , @NamedQuery(name = "Tanda.findByTabyIdandFecha", query = "SELECT b FROM Tanda t join  t.compras c join c.boletos b where t.taId =:taId and c.coFechacompra =:coFechacompra", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "Tanda.findByTaFhfin", query = "SELECT t FROM Tanda t WHERE t.taFhfin = :taFhfin")
    , @NamedQuery(name = "Tanda.findByFechas", query = "SELECT t FROM Tanda t WHERE t.taFhinicio <= :taFhfin <= t.taFhfin ")
    , @NamedQuery(name = "Tanda.findByTaVersion", query = "SELECT t FROM Tanda t WHERE t.taVersion = :taVersion")})
public class Tanda implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "CI_TANDA_ID_GENERATOR", sequenceName = "cineuna.CI_TANDA_SEQ05", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CI_TANDA_ID_GENERATOR")
    @Basic(optional = false)
//    @NotNull
    @Column(name = "TA_ID")
    private Long taId;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 20)
    @Column(name = "TA_IDIOMA")
    private String taIdioma;
    @Column(name = "TA_COSTO")
    private Integer taCosto;
    @Basic(optional = false)
//    @NotNull
    @Column(name = "TA_FHINICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date taFhinicio;
    @Basic(optional = false)
//    @NotNull
    @Column(name = "TA_FHFIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date taFhfin;
    @Version
//    @NotNull
    @Column(name = "TA_VERSION")
    private Long taVersion;
    @JoinColumn(name = "TA_ID_PEFK", referencedColumnName = "PE_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Pelicula taIdPefk;
    @JoinColumn(name = "TA_ID_SAFK", referencedColumnName = "SA_ID")
    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    private Sala taIdSafk;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "coTaId", fetch = FetchType.LAZY)
    private List<Compra> compras;

    public Tanda() {
    }

    public Tanda(Long taId) {
        this.taId = taId;
    }

    public Tanda(Long taId, String taIdioma, Date taFhinicio, Date taFhfin) {
        this.taId = taId;
        this.taIdioma = taIdioma;
        this.taFhinicio = taFhinicio;
        this.taFhfin = taFhfin;
    }
    public Tanda(TandaDto tanda){
        this.taId = tanda.getTaId();
        actualizar(tanda);
    }
    public void actualizar(TandaDto tanda){
        this.taCosto = tanda.getTaCosto();
        this.taFhfin = Date.from(LocalDateTime.parse(tanda.getTaFhfin()).atZone(ZoneId.systemDefault()).toInstant());
        this.taFhinicio = Date.from(LocalDateTime.parse(tanda.getTaFhinicio()).atZone(ZoneId.systemDefault()).toInstant());
        this.taIdioma = tanda.getTaIdioma();
    }
    public Long getTaId() {
        return taId;
    }

    public void setTaId(Long taId) {
        this.taId = taId;
    }

    public String getTaIdioma() {
        return taIdioma;
    }

    public void setTaIdioma(String taIdioma) {
        this.taIdioma = taIdioma;
    }

    public Integer getTaCosto() {
        return taCosto;
    }

    public void setTaCosto(Integer taCosto) {
        this.taCosto = taCosto;
    }

    public Date getTaFhinicio() {
        return taFhinicio;
    }

    public void setTaFhinicio(Date taFhinicio) {
        this.taFhinicio = taFhinicio;
    }

    public Date getTaFhfin() {
        return taFhfin;
    }

    public void setTaFhfin(Date taFhfin) {
        this.taFhfin = taFhfin;
    }

    public Pelicula getTaIdPefk() {
        return taIdPefk;
    }

    public void setTaIdPefk(Pelicula taIdPefk) {
        this.taIdPefk = taIdPefk;
    }

    public Sala getTaIdSafk() {
        return taIdSafk;
    }

    public void setTaIdSafk(Sala taIdSafk) {
        this.taIdSafk = taIdSafk;
    }

    @XmlTransient
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compraList) {
        this.compras = compraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taId != null ? taId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tanda)) {
            return false;
        }
        Tanda other = (Tanda) object;
        if ((this.taId == null && other.taId != null) || (this.taId != null && !this.taId.equals(other.taId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wscineuna.model.Tanda[ taId=" + taId + " ]";
    }

}
