/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.model;

import cr.ac.una.wscineuna.util.LocalDateAdapter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "PeliculaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PeliculaDto {

    private Long peId;
    private String peNombreEs;
    private String peNombreIn;
    private String peIdioma;
    private String peResennaEs;
    private String peResennaIn;
    private String peLinkEs;
    private String peLinkIn;
    private String peGenero;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate peFestreno;
//    private Image peImagen;
    private String peImagenS;
    private String peEstado;
    private List<TandaDto> tandas = new ArrayList();

    public PeliculaDto() {
    }

    public PeliculaDto(Pelicula pelicula) {
        this.peId = pelicula.getPeId();
        this.peNombreEs = pelicula.getPeNombreEs();
        this.peNombreIn = pelicula.getPeNombreIn();
        this.peIdioma = pelicula.getPeIdioma();
        this.peResennaEs = pelicula.getPeResennaEs();
        this.peResennaIn = pelicula.getPeResennaIn();
        this.peLinkEs = pelicula.getPeLinkEs();
        this.peLinkIn = pelicula.getPeLinkIn();
        this.peFestreno = pelicula.getPeFestreno().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        this.peEstado = pelicula.getPeEstado();
        this.peGenero = pelicula.getPeGenero();
    }

    public Long getPeId() {
        return peId;
    }

    public void setPeId(Long peId) {
        this.peId = peId;
    }

    public String getPeNombreEs() {
        return peNombreEs;
    }

    public void setPeNombreEs(String peNombreEs) {
        this.peNombreEs = peNombreEs;
    }

    public String getPeNombreIn() {
        return peNombreIn;
    }

    public void setPeNombreIn(String peNombreIn) {
        this.peNombreIn = peNombreIn;
    }

    public String getPeIdioma() {
        return peIdioma;
    }

    public String getPeGenero() {
        return peGenero;
    }

    public void setPeGenero(String peGenero) {
        this.peGenero = peGenero;
    }

    public void setPeIdioma(String peIdioma) {
        this.peIdioma = peIdioma;
    }

    public String getPeResennaEs() {
        return peResennaEs;
    }

    public void setPeResennaEs(String peResennaEs) {
        this.peResennaEs = peResennaEs;
    }

    public String getPeResennaIn() {
        return peResennaIn;
    }

    public void setPeResennaIn(String peResennaIn) {
        this.peResennaIn = peResennaIn;
    }

    public String getPeLinkEs() {
        return peLinkEs;
    }

    public void setPeLinkEs(String peLinkEs) {
        this.peLinkEs = peLinkEs;
    }

    public String getPeLinkIn() {
        return peLinkIn;
    }

    public void setPeLinkIn(String peLinkIn) {
        this.peLinkIn = peLinkIn;
    }

    public LocalDate getPeFestreno() {
        return peFestreno;
    }

    public void setPeFestreno(LocalDate peFestreno) {
        this.peFestreno = peFestreno;
    }

//    public Image getPeImagen() {
//        return peImagen;
//    }
//
//    public void setPeImagen(Image peImagen) {
//        this.peImagen = peImagen;
//    }

    public String getPeImagenS() {
        return peImagenS;
    }

    public void setPeImagenS(String peImagenS) {
        this.peImagenS = peImagenS;
    }

    public String getPeEstado() {
        return peEstado;
    }

    public void setPeEstado(String peEstado) {
        this.peEstado = peEstado;
    }

    public List<TandaDto> getTandas() {
        return tandas;
    }

    public void setTandas(List<TandaDto> tandaList) {
        this.tandas = tandaList;
    }

}
