/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.util.ConverArchivo;
import cr.ac.una.wscineuna.model.Pelicula;
import cr.ac.una.wscineuna.model.PeliculaDto;
import cr.ac.una.wscineuna.model.Tanda;
import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.GenerarUrl;
import cr.ac.una.wscineuna.util.Respuesta;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class PeliculaService {

    private static final Logger LOG = Logger.getLogger(PeliculaService.class.getName());
    //Cambiar la direccion de la carpeta de guardado aqui
    private String direccionG = GenerarUrl.direccionG();
    private String direccionR = GenerarUrl.direccionR();
    @PersistenceContext(unitName = "WsCineUNAPU")
    private EntityManager em;

    public Respuesta guardar(PeliculaDto peliculaDto) {
        try {
            String mensaje = "";
            Pelicula pelicula;
            if (peliculaDto.getPeId() != null && peliculaDto.getPeId() > 0) {
                pelicula = em.find(Pelicula.class, peliculaDto.getPeId());
                if (pelicula == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la pelicula a modificar.", "actualizarPelicula NoResultException");
                }
                pelicula.actualizar(peliculaDto);
                //elimiar imagen actual
                ConverArchivo.elimiarFile(pelicula.getPeImagen());
                //actualizar la imagen direccion de imagen
                BufferedImage buf = ConverArchivo.stringToBufferedImage(peliculaDto.getPeImagenS());
                String url = guardarImagen(buf, pelicula.getPeNombreEs());
                pelicula.setPeImagen(url);
                pelicula = em.merge(pelicula);
            } else {
                mensaje = "Actualizacion completa pelicula";
                pelicula = new Pelicula(peliculaDto);
                //guardar direccion de imagen
                BufferedImage buf = ConverArchivo.stringToBufferedImage(peliculaDto.getPeImagenS());
                String url = guardarImagen(buf, pelicula.getPeNombreEs());
                if (url != null) {
                    pelicula.setPeImagen(url);
                }
                em.persist(pelicula);
            }
            em.flush();
            PeliculaDto peli = new PeliculaDto(pelicula);
            //recuperar imagen
            String imCode = ConverArchivo.fileToString(pelicula.getPeImagen());
            if (imCode != null) {
                peli.setPeImagenS(imCode);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Pelicula", peli);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la pelicula. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoPelicula " + e.getMessage());
        }
    }

    /**
     * Crea el archivo de imagen en la carpeta de imagenes
     *
     * @param im
     * @return
     * @throws IOException
     */
    public String guardarImagen(BufferedImage im, String n) throws IOException {
        try {
            String replace = n.replace(" ", "");

            File archivImagen = new File(direccionG + replace + ".png");
            ImageIO.write(im, "png", archivImagen);
            return (direccionR + replace + ".png");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la pelicula. " + e.getMessage(), e);
            return null;
        }
    }

    public Respuesta getPeliculasList() {
        try {
            Query qryGetPeliculas = em.createNamedQuery("Pelicula.findAll", Pelicula.class);
            List<Pelicula> peliculas = qryGetPeliculas.getResultList();
            List<PeliculaDto> peliculasDto = new ArrayList<>();
            for (Pelicula peli : peliculas) {
                PeliculaDto peliculaDto = new PeliculaDto(peli);
                String imS = ConverArchivo.fileToString(peli.getPeImagen());
                peliculaDto.setPeImagenS(imS);
                peliculasDto.add(peliculaDto);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Peliculas", peliculasDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getPeliculas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las peliculas.", "getPeliculas " + ex.getMessage());
        }
    }

    public Respuesta getPeliculasFiltroG(String genero) {
        try {
            if (genero != null && !genero.isEmpty()) {
                Query qryGetPeliculas = em.createNamedQuery("Pelicula.findByGenero", Pelicula.class);
                qryGetPeliculas.setParameter("peGenero", genero + "%");
                List<Pelicula> peliculas = qryGetPeliculas.getResultList();
                List<PeliculaDto> peliculasDto = new ArrayList<>();
                for (Pelicula peli : peliculas) {
                    peliculasDto.add(new PeliculaDto(peli));
                }
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Peliculas", peliculasDto);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar un genero", "getPeliculas NoResultException");
            }
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getPeliculas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las peliculas.", "getPeliculas " + ex.getMessage());
        }
    }

    public Respuesta getGanancias(Long id) {
        try {
            Pelicula pelicula;
            if (id != null && id > 0) {
                pelicula = em.find(Pelicula.class, id);
                if (pelicula == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la pelicula.", "getPelicula NoResultException");
                }
                List<TandaDto> tandas = new ArrayList();
                for (Tanda t : pelicula.getTandas()) {
                    TandaDto tandaDto = new TandaDto(t);
//                    for (Boleto b : t.getBoletos()) {
//                        BoletoDto boletoDto = new BoletoDto(b);
//                        tandaDto.getBoletos().add(boletoDto);
//                    }
                    tandas.add(tandaDto);
                }

                PeliculaDto peliculaDto = new PeliculaDto(pelicula);
                peliculaDto.setTandas(tandas);
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Pelicula", peliculaDto);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar un elemento", "getPeliculas NoResultException");
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la pelicula.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la pelicula.", "getPeliculas " + e.getMessage());
        }
    }
}
