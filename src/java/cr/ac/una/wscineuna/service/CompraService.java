/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.model.Boleto;
import cr.ac.una.wscineuna.model.BoletoDto;
import cr.ac.una.wscineuna.model.Compra;
import cr.ac.una.wscineuna.model.CompraDto;
import cr.ac.una.wscineuna.model.Tanda;
import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.model.Usuario;
import cr.ac.una.wscineuna.model.UsuarioDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class CompraService {

    private static final Logger LOG = Logger.getLogger(CompraService.class.getName());
    //unidad de persistancia
    @PersistenceContext(unitName = "WsCineUNAPU")
    private EntityManager em;

    public Respuesta guardarCompra(CompraDto compraDto) {
        try {
            String mensaje = "";
            Compra compra;
            Usuario usuario;
            Tanda tanda;
            if (compraDto.getCoId() != null && compraDto.getCoId() > 0) {
                tanda = em.find(Tanda.class, compraDto.getCoTaId().getTaId());
                usuario = em.find(Usuario.class, compraDto.getCoUsid().getUsId());
                compra = em.find(Compra.class, compraDto.getCoId());
                if (compra == null || tanda == null || usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la compra o uno de los registros asiciados", "actualizarBoleto NoResultException");
                }
                compra.actualizar(compraDto);
                //actualizar compra
                for (BoletoDto b : compraDto.getBoletos()) {
                    if (b.getBoId() == null) {
                        Boleto boleto = new Boleto(b);
                        compra.getBoletos().add(boleto);
                        boleto.setBoCoid(compra);
                    } else {
                        if (b.getModificado()) {
                            Boleto bole = em.find(Boleto.class, b.getBoId());
                            bole.actualizar(b);

                        }
                    }
                }
                compra = em.merge(compra);
            } else {
                //conexion
                compra = new Compra(compraDto);
                usuario = em.find(Usuario.class, compraDto.getCoUsid().getUsId());
                usuario.getCompras().add(compra);
                compra.setCoUsid(usuario);
                tanda = em.find(Tanda.class, compraDto.getCoTaId().getTaId());
                tanda.getCompras().add(compra);
                compra.setCoTaId(tanda);

                em.persist(compra);

            }
            CompraDto compraD = new CompraDto(compra);
            compraD.setCoTaId(new TandaDto(tanda));
            compraD.setCoUsid(new UsuarioDto(usuario));
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Compra", compraD);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al actualizar el boleto. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el actualizado.", "actualizar" + e.getMessage());
        }
    }

    /**
     * regresa la liste de bolestos para un mismo dia y una misma tanda
     *
     * @param fecha
     * @return
     */
    public Respuesta getCompras(Long idtanda, LocalDate fecha) {
        try {
            if (idtanda == null && idtanda <= 0) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la tanda a modificar.", "actualizarTanda NoResultException");
            }

            Query qryGetBoletos = em.createNamedQuery("Tanda.findByTabyIdandFecha", Tanda.class);
            Date fec = Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
            qryGetBoletos.setParameter("taId", idtanda);
            qryGetBoletos.setParameter("coFechacompra", fec);
            List<Boleto> boletos = qryGetBoletos.getResultList();
            List<BoletoDto> bolestosDto = new ArrayList<>();
            for (Boleto b : boletos) {
                BoletoDto bol = new BoletoDto(b);
                bolestosDto.add(bol);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Boletos", bolestosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getPeliculas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las peliculas.", "getPeliculas " + ex.getMessage());
        }
    }

    public Respuesta guardarBoleto(BoletoDto bol) {
        try {
            String mensaje = "";
            Boleto boleto;
            if (bol.getBoId() != null && bol.getBoId() > 0) {
                boleto = em.find(Boleto.class, bol.getBoId());
                if (boleto == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el boleto a modificar.", "actualizarBoleto NoResultException");
                }
                boleto.actualizar(bol);
                boleto = em.merge(boleto);
            } else {
                mensaje = "Actualizacion completa";
                //buscar compra
                Compra compra = em.find(Compra.class, bol.getIdCompra());
                if (compra == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el registro de la compra a modificar.", "actualizarCompra NoResultException");
                }
                boleto = new Boleto(bol);
                compra.getBoletos().add(boleto);
                boleto.setBoCoid(compra);
                compra = em.merge(compra);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Boleto", new BoletoDto(boleto));
        } catch (Exception e) {

            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el boleto. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoBoleto " + e.getMessage());
        }
    }

    /**
     * Recibe una compra y la elimina
     *
     * @param compraD
     * @return
     */
    public Respuesta elimiarCompra(Long idCom) {
        try {
            Compra compra;
            if (idCom != null && idCom > 0) {
                compra = em.find(Compra.class, idCom);
                if (compra == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el registro a elimiar.", "elimiarCompra NoResultException");
                }
                for (Boleto b : compra.getBoletos()) {
                    if (b.getBoId() != null && b.getBoId() > 0) {
                        Boleto bol = em.find(Boleto.class, b.getBoId());
                        em.remove(bol);
                    }
                }
                em.remove(compra);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el registro a elimiar.", "elimiarCompra NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el registro .", "elimiarCompra " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el registro.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el registro.", "elimiarCompra " + ex.getMessage());
        }
    }
}
