/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.model.Usuario;
import cr.ac.una.wscineuna.model.UsuarioDto;
import cr.ac.una.wscineuna.util.Clave;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class UsuarioService {

    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());
    //unidad de persistancia
    @PersistenceContext(unitName = "WsCineUNAPU")

    private EntityManager em;

    public Respuesta getUsuaro(String usuario, String clave) {
        try {
            Query qryGetUsuario = em.createNamedQuery("Usuario.findByUsuClave", Usuario.class);
            qryGetUsuario.setParameter("usUsuario", usuario);
            qryGetUsuario.setParameter("usClave", clave);
            Usuario usuario1 = (Usuario) qryGetUsuario.getSingleResult();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario1));
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe el usuario con los datos ingresados ", "getUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.2", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrion un error al consultar el usuario.2", "getUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrion un error al consultar el usuario.1", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.1", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta cambiarClave(String usuario, String nuClave) {
        try {
            Query qryGetUsuario = em.createNamedQuery("Usuario.findByUsUsuario", Usuario.class);
            qryGetUsuario.setParameter("usUsuario", usuario);
            Usuario usuario1 = (Usuario) qryGetUsuario.getSingleResult();
            if (usuario1 == null) {

                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el usuario a modificar.", "recupearClaveUsuario NoResultException");
            }
            usuario1.setUsRecuperacionclave("S");
            usuario1.setUsClave(nuClave);
            usuario1 = em.merge(usuario1);
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario1));
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe el usuario con los datos ingresados ", "getUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.2", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrion un error al consultar el usuario.2", "getUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrion un error al consultar el usuario.1", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.1", "getUsuario " + ex.getMessage());
        }
    }

    public void activarUsuario(String codigo) {
        try {
            Query qryGetUsuarioAc = em.createNamedQuery("Usuario.findByUaCodigoActivacion", Usuario.class);
            qryGetUsuarioAc.setParameter("uaCodigoActivacion", codigo);
            Usuario usuario1 = (Usuario) qryGetUsuarioAc.getSingleResult();
            if (usuario1 == null) {
                return;
            }
            usuario1.setUsActivo("A");
            usuario1.setUsRecuperacionclave("N");
            usuario1 = em.merge(usuario1);
            em.flush();
        } catch (NoResultException ex) {
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.2", ex);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrion un error al consultar el usuario.1", ex);
        }
    }

    public Respuesta validarUsuario(String usuario) {
        try {
            Query qryGetUsuario = em.createNamedQuery("Usuario.findByUsUsuario", Usuario.class);
            qryGetUsuario.setParameter("usUsuario", usuario);
            Usuario usuario1 = (Usuario) qryGetUsuario.getSingleResult();

            if (usuario1 == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el usuario a modificar.", "recupearClaveUsuario NoResultException");
            }
            usuario1 = em.merge(usuario1);
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario1));
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe el usuario con los datos ingresados ", "getUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.2", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrion un error al consultar el usuario.2", "getUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrion un error al consultar el usuario.1", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.1", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta guardar(UsuarioDto usuarioDto) {
        try {
            String mensaje = "";
            Usuario usuario;
            if (usuarioDto.getUsId() != null && usuarioDto.getUsId() > 0) {
                usuario = em.find(Usuario.class, usuarioDto.getUsId());
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el usuario a modificar.", "actualizarUsuario NoResultException");
                }
                usuario.actualizar(usuarioDto);
                usuario = em.merge(usuario);
            } else {
                mensaje = "Actualizacion completa";
                usuario = new Usuario(usuarioDto);
                usuario.setUaCodigoActivacion(Clave.getInstance().codigoRandom());
                em.persist(usuario);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Usuario", new UsuarioDto(usuario));
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "El usuario ingresado ya existe, por favor cambielo e intente de nuevo.", "guardarUsuario " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoUsuario " + e.getMessage());
        }
    }

    public Respuesta elimiarUsuario(Long id) {
        try {
            Usuario usuario;
            if (id != null && id > 0) {
                usuario = em.find(Usuario.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el usuario a elimiar.", "elimiarUsuario NoResultException");
                }
                em.remove(usuario);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario NoResultException");

            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el usuario .", "eliminarUsuario " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

    public Respuesta modificarAdministrador(Long id) {
        try {
            Usuario usuario;
            if (id != null && id > 0) {
                usuario = em.find(Usuario.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el usuario a elimiar.", "elimiarUsuario NoResultException");
                }
                if (usuario.getUsAdministrador().equalsIgnoreCase("A")) {
                    usuario.setUsActivo("N");
                } else {
                    usuario.setUsActivo("A");
                }
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario NoResultException");

            }

        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el usuario .", "eliminarUsuario " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

    public Respuesta getUsuariosActivos() {
        try {
            Query qryGetUsuarios = em.createNamedQuery("Usuario.findAll", Usuario.class);
            List<Usuario> usuarios = qryGetUsuarios.getResultList();
            List<UsuarioDto> usuariosDto = new ArrayList<>();
            for (Usuario us : usuarios) {
                if (us.getUsActivo().equals("A")) {
                    UsuarioDto usuarioDto = new UsuarioDto(us);
                    usuariosDto.add(usuarioDto);
                }
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuarios", usuariosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getUsuarios NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los usuarios.", "getUsuarios " + ex.getMessage());
        }
    }
}
