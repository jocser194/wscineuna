/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.model.Asiento;
import cr.ac.una.wscineuna.model.AsientoDto;
import cr.ac.una.wscineuna.model.PeliculaDto;
import cr.ac.una.wscineuna.model.Sala;
import cr.ac.una.wscineuna.model.SalaDto;
import cr.ac.una.wscineuna.model.Tanda;
import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.ConverArchivo;
import cr.ac.una.wscineuna.util.GenerarUrl;
import cr.ac.una.wscineuna.util.Respuesta;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class SalaService {

    private static final Logger LOG = Logger.getLogger(SalaService.class.getName());
//Cambiar la direccion de la carpeta de guardado aqui

    private String direccionG = GenerarUrl.direccionG();
    private String direccionR = GenerarUrl.direccionR();

    ClassLoader di = Thread.currentThread().getContextClassLoader();

    @PersistenceContext(unitName = "WsCineUNAPU")

    private EntityManager em;

    public Respuesta guardar(SalaDto salaDto) {
        try {
            String mensaje = "";
            Sala sala;
            if (salaDto.getSaId() != null && salaDto.getSaId() > 0) {

                sala = em.find(Sala.class, salaDto.getSaId());
                if (sala == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala a modificar.", "actualizarPelicula NoResultException");
                }

                sala.actualizar(salaDto);
                for (AsientoDto as : salaDto.getAsientos()) {
                    if (as.getAsId() == null) {
                        Asiento a = new Asiento(as);
                        sala.getAsientos().add(a);
                        a.setAsIdSala(sala);
                    }else{
                        if(as.getModificado()){
                            Asiento a = em.find(Asiento.class, as.getAsId());
                            a.actualizr(as);
                        }
                    }
                }
                ConverArchivo.elimiarFile(sala.getSaFonImagen());
                BufferedImage imC = ConverArchivo.stringToBufferedImage(salaDto.getSaFonImagen());
                String dir = guardarImagen(imC, sala.getSaNombre());
                sala.setSaFonImagen(dir);
                sala = em.merge(sala);
            } else {

                mensaje = "Actualizacion completa sala";
                sala = new Sala(salaDto);
                BufferedImage imC = ConverArchivo.stringToBufferedImage(salaDto.getSaFonImagen());
                String dir = guardarImagen(imC, salaDto.getSaNombre());
                sala.setSaFonImagen(dir);
                em.persist(sala);
            }

            SalaDto salaD = new SalaDto(sala);
            for (Asiento a : sala.getAsientos()) {
                AsientoDto as = new AsientoDto(a);
                salaD.getAsientos().add(as);
            }
            String imCode = ConverArchivo.fileToString(sala.getSaFonImagen());
            salaD.setSaFonImagen(imCode);

            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Sala", salaD);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoPelicula " + e.getMessage());
        }
    }

    public Respuesta cargarAsientos(Long idSala, List<AsientoDto> asientos) {
        try {
            String mensaje = "";
            Sala sala;
            SalaDto sDto = new SalaDto();
            if (idSala != null && idSala > 0) {

                sala = em.find(Sala.class, idSala);
                if (sala == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala a modificar.", "actualizarPelicula NoResultException");
                }
                for (AsientoDto as : asientos) {
                    Asiento a = new Asiento(as);
                    sala.getAsientos().add(a);
                    a.setAsIdSala(sala);
                }
                sala = em.merge(sala);
                sDto = new SalaDto(sala);
                for (Asiento s : sala.getAsientos()) {
                    AsientoDto a = new AsientoDto(s);
                    sDto.getAsientos().add(a);
                }
            }

            em.flush();

            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Sala", sDto);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoPelicula " + e.getMessage());
        }
    }

    public String guardarImagen(BufferedImage im, String n) throws IOException {
        try {
            String replace = n.replace(" ", "");
            File archivImagen = new File(direccionG + replace + ".png");
            ImageIO.write(im, "png", archivImagen);
            return (direccionR + replace + ".png");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la pelicula. " + e.getMessage(), e);
            return null;
        }
    }

    public Respuesta getSala(Long id) {
        try {
            Sala sala;
            SalaDto salaDto = new SalaDto();
            if (id != null && id > 0) {
                sala = em.find(Sala.class, id);
                if (sala == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala.", "getSala NoResultException");
                }
                salaDto = new SalaDto(sala);
                for (Asiento a : sala.getAsientos()) {
                    AsientoDto asiento = new AsientoDto(a);
                    salaDto.getAsientos().add(asiento);
                }
                String imS = ConverArchivo.fileToString(sala.getSaFonImagen());
                salaDto.setSaFonImagen(imS);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la sala.", "getSala NoResultException");
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Sala", salaDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getSala NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las sala.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las sala.", "getSala " + ex.getMessage());
        }
    }

    public Respuesta getSalas() {
        try {
            Query qryGetSala = em.createNamedQuery("Sala.findAll", Sala.class);
            List<Sala> salas = qryGetSala.getResultList();
            List<SalaDto> salasDto = new ArrayList<>();
            for (Sala s : salas) {
                SalaDto sala = new SalaDto(s);
                for (Asiento a : s.getAsientos()) {
                    AsientoDto as = new AsientoDto(a);
                    sala.getAsientos().add(as);
                }
                for (Tanda t : s.getTandas()) {
                    TandaDto tan = new TandaDto(t);
                    PeliculaDto peli = new PeliculaDto(t.getTaIdPefk());
                    tan.setTaIdPefk(peli);
                    sala.getTandas().add(tan);
                }
                String imS = ConverArchivo.fileToString(s.getSaFonImagen());
                sala.setSaFonImagen(imS);
                salasDto.add(sala);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Salas", salasDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getSalas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las salas.", "getSalas " + ex.getMessage());
        }
    }

    public Respuesta eliminarSalas(List<SalaDto> salas) {
        try {
            if (!salas.isEmpty()) {
                for (SalaDto s : salas) {
                    if (s.getSaId() != null && s.getSaId() > 0) {
                        Sala sala = em.find(Sala.class, s.getSaId());
                        if (sala == null) {
                            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala: " + sala.getSaId(), "elimiarSala NoResultException");
                        }
                        ConverArchivo.elimiarFile(sala.getSaFonImagen());
                        em.remove(s);
                    } else {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala: ", "elimiarSala NoResultException");
                    }
                }
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar elementos a eliminar.", "eliminarSalas NoResultException");

            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "");

        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el usuario .", "eliminarUsuario " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

}
