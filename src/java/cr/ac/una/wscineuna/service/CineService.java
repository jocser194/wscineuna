/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.model.Cine;
import cr.ac.una.wscineuna.model.CineDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class CineService {

    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());
    //unidad de persistancia
    @PersistenceContext(unitName = "WsCineUNAPU")

    private EntityManager em;

    public Respuesta guardar(CineDto cineDto) {
        try {
            String mensaje = "";
            Cine cine;
            if (cineDto.getCid() != null && cineDto.getCid() > 0) {
                cine = em.find(Cine.class, cineDto.getCid());
                if (cine == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro el cine a modificar.", "actualizarCine NoResultException");
                }
                cine.actualizar(cineDto);
                cine = em.merge(cine);
            } else {
                mensaje = "Actualizacion completa";
                cine = new Cine(cineDto);
                em.persist(cine);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Usuario", new CineDto(cine));
        } catch (Exception e) {

            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el cine. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro del cine." + e.getMessage(), "guardadoCine" + e.getMessage());
        }
    }

    public Respuesta getCines() {
        try {
            Query qryCine = em.createNamedQuery("Cine.findAll", Cine.class);
            List<Cine> cines = qryCine.getResultList();
            List<CineDto> cineDto = new ArrayList();

            for (Cine c : cines) {
                CineDto ci = new CineDto(c);
                cineDto.add(ci);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Cines", cineDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un cine con el código ingresado.", "getCine NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el cine.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el cine.", "getCine NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el empleado.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el cine.", "getCine " + ex.getMessage());
        }
    }
}
