/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.service;

import cr.ac.una.wscineuna.model.Asiento;
import cr.ac.una.wscineuna.model.AsientoDto;
import cr.ac.una.wscineuna.model.Boleto;
import cr.ac.una.wscineuna.model.BoletoDto;
import cr.ac.una.wscineuna.model.Compra;
import cr.ac.una.wscineuna.model.CompraDto;
import cr.ac.una.wscineuna.model.Pelicula;
import cr.ac.una.wscineuna.model.Sala;
import cr.ac.una.wscineuna.model.SalaDto;
import cr.ac.una.wscineuna.model.Tanda;
import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.ConverArchivo;
import cr.ac.una.wscineuna.util.Respuesta;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matami
 */
@Stateless
@LocalBean
public class TandaService {

    private static final Logger LOG = Logger.getLogger(TandaService.class.getName());
    //unidad de persistancia
    @PersistenceContext(unitName = "WsCineUNAPU")

    private EntityManager em;

    public Respuesta guardar(TandaDto tandaDto) {
        try {
            String mensaje = "";
            Tanda tanda;
            if (tandaDto.getTaId() != null && tandaDto.getTaId() > 0) {
                tanda = em.find(Tanda.class, tandaDto.getTaId());
                if (tanda == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la tanda a modificar.", "actualizarTanda NoResultException");
                }
                tanda.actualizar(tandaDto);
                tanda = em.merge(tanda);
            } else {
                tanda = new Tanda(tandaDto);
                //lista de boletos
//                if (!tandaDto.getCompras().isEmpty()) {
////                    for (BoletoDto b : tandaDto.getBoletos()) {
////                        Boleto boleto = new Boleto(b);
////                        boleto.setBoIdTanda(tanda);
////                        tanda.getBoletos().add(boleto);
////                    }
//                } else {
//                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontraron boletos para guardar.", "guardarBoletos NoResultException");
//                }
                //agrega la pelicula
                if (tandaDto.getTaIdPefk() != null) {
                    Pelicula peli = em.find(Pelicula.class, tandaDto.getTaIdPefk().getPeId());
                    peli.getTandas().add(tanda);
                    tanda.setTaIdPefk(peli);
                } else {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro pelicula asociada.", "actualizarPelicula NoResultException");
                }

                //agregado a la sala
                if (tandaDto.getTaIdSafk() != null) {
                    Sala sala = em.find(Sala.class, tandaDto.getTaIdSafk().getSaId());
                    sala.getTandas().add(tanda);
                    tanda.setTaIdSafk(sala);
                } else {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la sala a la que pertenece esta tanda.", "actualizarSala NoResultException");
                }
                mensaje = "Actualizacion completa tanda";
                tanda = new Tanda(tandaDto);
                em.persist(tanda);
            }
            em.flush();
            TandaDto tan = new TandaDto(tanda);
            tan.setTaIdPefk(tandaDto.getTaIdPefk());
            tan.setTaIdSafk(tandaDto.getTaIdSafk());
            return new Respuesta(true, CodigoRespuesta.CORRECTO, mensaje, "", "Tanda", tan);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la tanda. ", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error durante el guardaro1.", "guardadoTanda" + e.getMessage());
        }
    }

    /**
     * Lista de tandas con el id de una pelicula en especifico
     *
     * @param idpeli
     * @return
     */
    public Respuesta getTandas(Long idpeli) {
        try {
            Pelicula pelicula;
            List<TandaDto> tandas = new ArrayList<>();
            if (idpeli != null && idpeli > 0) {
                pelicula = em.find(Pelicula.class, idpeli);
                if (pelicula == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontro la pelicula de a cual se desea conocer las tandas.", "getTanda NoResultException");
                }
                for (Tanda t : pelicula.getTandas()) {
                    TandaDto tanda = new TandaDto(t);
//                    for (Compra c : t.getCompras()) {
////                        CompraDto com = new CompraDto(c);
////                        for (Boleto b : c.getBoletos()) {
////                            BoletoDto bo = new BoletoDto(b);
////                            com.getBoletos().add(bo);
////                        }
////                        tanda.getCompras().add(com);
//                    }
                    Sala sala = t.getTaIdSafk();
                    SalaDto salaDto = new SalaDto(sala);
                    for (Asiento ass : sala.getAsientos()) {
                        AsientoDto asDto = new AsientoDto(ass);
                        salaDto.getAsientos().add(asDto);
                    }
                    String imS = ConverArchivo.fileToString(sala.getSaFonImagen());
                    salaDto.setSaFonImagen(imS);
                    tanda.setTaIdSafk(salaDto);
                    tandas.add(tanda);
                }
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe introducir un id de la pelicula.", "getTandas NoResultException");
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Tandas", tandas);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getTandas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las tandas.", "getTandas " + ex.getMessage());
        }
    }

    public Respuesta getTandasFiltroFecha(LocalDateTime fhIni) {
        try {

            Query qryGetTandas = em.createNamedQuery("Tanda.findByFechas", Tanda.class);
            qryGetTandas.setParameter("taFhfin", fhIni);
            List<Tanda> tandas = qryGetTandas.getResultList();
            List<TandaDto> tandasDto = new ArrayList<>();
            for (Tanda t : tandas) {
//                TandaDto tan = new TandaDto(t);
//                for (Boleto b : t.getBoletos()) {
//                    BoletoDto boleto = new BoletoDto(b);
//                    tan.getBoletos().add(boleto);
//                }
//                PeliculaDto peli = new PeliculaDto(t.getTaIdPefk());
//                tan.setTaIdPefk(peli);
//                tandasDto.add(tan);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Tandas", tandasDto);
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen registros de este tipo", "getTandas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consular las peliculas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las tandas.", "getTandas " + ex.getMessage());
        }
    }
}
