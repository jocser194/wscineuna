/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.PeliculaDto;
import cr.ac.una.wscineuna.model.ReporteDto;
import cr.ac.una.wscineuna.resources.GenrarReporte;
import cr.ac.una.wscineuna.service.PeliculaService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matami
 */
@Path("PeliculaController")
public class PeliculaController {

    @EJB
    PeliculaService peliculaService;

    ClassLoader di = Thread.currentThread().getContextClassLoader();

    @POST
    @Path("/guardarPelicula")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPelicula(PeliculaDto pelicula) {
        try {
            Respuesta res = peliculaService.guardar(pelicula);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensajeInterno()).build();
            }
            return Response.ok((PeliculaDto) res.getResultado("Pelicula")).build();
        } catch (Exception e) {
            Logger.getLogger(PeliculaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar la pelicula").build();
        }
    }

    @GET
    @Path("/reporte/{idPeli}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response reporte(@PathParam("idPeli") Long idPeli) {

        try {
            GenrarReporte con = new GenrarReporte();
            Respuesta res = con.generarReportePelicualas(idPeli);

            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensajeInterno()).build();
            }
            return Response.ok((ReporteDto) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(PeliculaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las peliculas").build();
        }
    }
    @GET
    @Path("/getPeliculasList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getPeliculasList() {
        try {
            Respuesta res = peliculaService.getPeliculasList();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<PeliculaDto>>((List<PeliculaDto>) res.getResultado("Peliculas")) {
            }).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(PeliculaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las peliculas").build();
        }
    }

    @GET
    @Path("/getPeliculasFiltroG/{genero}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getPeliculasFiltroG(@PathParam("genero") String genero) {
        try {
            Respuesta res = peliculaService.getPeliculasFiltroG(genero);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<PeliculaDto>>((List<PeliculaDto>) res.getResultado("Peliculas")) {
            }).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(PeliculaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las peliculas").build();
        }
    }

    /**
     * retorna una pelicula con con los datos para crear los reportes
     *
     * @param id
     * @return
     */
    @GET
    @Path("/getGanancias/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getPeliculasFiltroG(@PathParam("id") Long id) {
        try {
            Respuesta res = peliculaService.getGanancias(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            //codigo para generar los reportes
            return Response.ok((PeliculaDto) res.getResultado("Pelicula")).build();
        } catch (Exception ex) {
            Logger.getLogger(PeliculaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las peliculas").build();
        }
    }
}
