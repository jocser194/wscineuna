/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.ReporteDto;
import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.resources.GenrarReporte;
import cr.ac.una.wscineuna.service.TandaService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matami
 */
@Path("TandaController")
public class TandaController {

    @EJB
    TandaService tandaService;

    @POST
    @Path("/guardarTanda")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarTanda(TandaDto tandaDto) {
        try {
            Respuesta res = tandaService.guardar(tandaDto);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensajeInterno()).build();
            }
            return Response.ok((TandaDto) res.getResultado("Tanda")).build();
        } catch (Exception e) {
            Logger.getLogger(TandaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar la tanda").build();
        }
    }

    
    @GET
    @Path("/getTandas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getPeliculasFiltroG(@PathParam("id") Long id) {
        try {
            Respuesta res = tandaService.getTandas(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<TandaDto>>((List<TandaDto>) res.getResultado("Tandas")) {
            }).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(TandaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las tandas de las peliculas ").build();
        }
    }

    @GET
    @Path("/getTandasFiltroFecha/{fini}/{ffin}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTandasFiltroFecha(@PathParam("fecandHora") String fecandHora) {
        try {
            LocalDateTime lda = LocalDateTime.parse(fecandHora);
            Respuesta res = tandaService.getTandasFiltroFecha(lda);//vamos por aqui
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<TandaDto>>((List<TandaDto>) res.getResultado("Tandas")) {
            }).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(TandaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las tandas").build();
        }
    }

    @POST
    @Path("/reporteTanda")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response reporteTanda(ReporteDto reporte) {

        try {
            GenrarReporte con = new GenrarReporte();
            Respuesta res = con.generarReporteTandas(reporte);

            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensajeInterno()).build();
            }
            return Response.ok((ReporteDto) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(TandaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las peliculas").build();
        }
    }
}
