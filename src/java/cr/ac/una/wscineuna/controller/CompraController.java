/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.BoletoDto;
import cr.ac.una.wscineuna.model.CompraDto;
import cr.ac.una.wscineuna.model.ReporteDto;
import cr.ac.una.wscineuna.model.UsuarioDto;
import cr.ac.una.wscineuna.resources.GenrarReporte;
import cr.ac.una.wscineuna.service.CompraService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matami
 */
@Path("CompraController")
public class CompraController {

    @EJB
    CompraService compraService;

    @GET
    @Path("/getCompras/{idTanda}/{fecha}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCompras(@PathParam("idTanda") Long idTanda,@PathParam("fecha") String fecha) {
        try {
            LocalDate ld = LocalDate.parse(fecha);
            Respuesta respuesta = compraService.getCompras(idTanda,ld);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<BoletoDto>>((List<BoletoDto>) respuesta.getResultado("Boletos")) {
            }).build();

        } catch (Exception ex) {
            Logger.getLogger(CompraController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el boleto").build();
        }
    }

    @POST
    @Path("/guardarCompra")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarCompra(CompraDto compra) {
        try {
            Respuesta res = compraService.guardarCompra(compra);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }

            return Response.ok((CompraDto) res.getResultado("Compra")).build();
        } catch (Exception e) {
            Logger.getLogger(CompraController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar el archivo de compra").build();
        }
    }
    
    @POST
    @Path("/guardarBoleto")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarBoleto(BoletoDto boleto) {
        try {
            Respuesta res = compraService.guardarBoleto(boleto);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }

            return Response.ok((BoletoDto) res.getResultado("Boleto")).build();
        } catch (Exception e) {
            Logger.getLogger(CompraController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar el boleto").build();
        }
    }
    @POST
    @Path("/confirmarCompra/{mensaje}/{nombreArch}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
     public Response confirmarCompra(CompraDto compra,@PathParam("mensaje") String mensaje,@PathParam("nombreArch") String nombreArch) {
        try {
            Respuesta res = compraService.guardarCompra(compra);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            CompraDto comp = (CompraDto)res.getResultado("Compra");
            GenrarReporte con = new GenrarReporte();
            Respuesta r =  con.generarReporteCompra(comp.getCoId(), comp.getCoUsid().getUsCorreo(),mensaje ,nombreArch );
            return Response.ok((ReporteDto) r.getResultado("Comprobante")).build();
        } catch (Exception e) {
            Logger.getLogger(CompraController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al enviar el comprobante").build();
        }
    }
    @DELETE
    @Path("/elimiarCompra/{idCom}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response elimiarCompra(@PathParam("idCom")Long idCom) {
        try {
            Respuesta respuesta = compraService.elimiarCompra(idCom);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();

        } catch (Exception ex) {
            Logger.getLogger(CompraController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el registro de compra").build();
        }
    }

}
