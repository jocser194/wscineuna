/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.TandaDto;
import cr.ac.una.wscineuna.model.UsuarioDto;
import cr.ac.una.wscineuna.service.TandaService;
import cr.ac.una.wscineuna.service.UsuarioService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.push.Push;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matami
 */
@Path("UsuarioController")
public class UsuarioController {

    @EJB
    UsuarioService usuarioService;

    /**
     * Verifica si el usuario existe y lo retorna de ser asi de lo contrarion
     * devuelve el mensaje de error respectivo
     *
     * @param usuario
     * @param clave
     * @return
     */
    @GET//obtener
    //nombre metodo / var 1   / var 2
    @Path("/getUsuario/{usuario}/{clave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuario(@PathParam("usuario") String usuario, @PathParam("clave") String clave) {
        try {
            Respuesta respuesta = usuarioService.getUsuaro(usuario, clave);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el usuario").build();
        }
    }
      @GET//obtener
    //nombre metodo / var 1   / var 2
    @Path("/recuperarClave/{usuario}/{nuClave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response recuperarClave(@PathParam("usuario") String usuario, @PathParam("nuClave") String nuClave) {
        try {
            Respuesta respuesta = usuarioService.cambiarClave(usuario, nuClave);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el usuario").build();
        }
    }

    @GET
    @Path("/activar/{codigo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void activar(@PathParam("codigo") String codigo) {
        try {
            usuarioService.activarUsuario(codigo);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @GET
    @Path("/validarUsuario/{usuario}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validarUsuario(@PathParam("usuario") String usuario) {
        try {
            Respuesta res = usuarioService.validarUsuario(usuario);
         if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) res.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el usuario").build();
        }
    }
    @POST
    @Path("/guardar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardar(UsuarioDto usuario) {
        try {
            Respuesta res = usuarioService.guardar(usuario);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) res.getResultado("Usuario")).build();
        } catch (Exception e) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar el usuario").build();
        }
    }

    @DELETE
    @Path("/elimiarUsuario/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuario(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = usuarioService.elimiarUsuario(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();

        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el usuario").build();
        }
    }

    @Push
    @Path("/modificarAdministrador/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificarAdministrador(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = usuarioService.modificarAdministrador(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((UsuarioDto) respuesta.getResultado("Usuario")).build();

        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el usuario").build();
        }
    }
    @GET
    @Path("/getUsuariosActivos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getUsuariosActivos() {
        try {
            Respuesta res = usuarioService.getUsuariosActivos();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<UsuarioDto>>((List<UsuarioDto>) res.getResultado("Usuarios")){}).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios.").build();
        }
    }
//    
}
