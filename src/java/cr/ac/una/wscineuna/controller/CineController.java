/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.CineDto;
import cr.ac.una.wscineuna.service.CineService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matami
 */
@Path("CineController")
public class CineController {

    @EJB
    CineService cineService;

    @GET
    @Path("/getCines")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCines() {
        try {
            Respuesta respuesta = cineService.getCines();
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<CineDto>>((List<CineDto>) respuesta.getResultado("Cines")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(CineController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar el cine").build();
        }
    }

    @POST
    @Path("/guardarCine")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarCine(CineDto cine) {
        try {
            Respuesta res = cineService.guardar(cine);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((CineDto) res.getResultado("Cine")).build();
        } catch (Exception e) {
            Logger.getLogger(CineController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar el cine").build();
        }
    }
}
