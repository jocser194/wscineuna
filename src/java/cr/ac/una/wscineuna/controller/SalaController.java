/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.controller;

import cr.ac.una.wscineuna.model.AsientoDto;
import cr.ac.una.wscineuna.model.SalaDto;
import cr.ac.una.wscineuna.service.SalaService;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 *
 * @author Matami
 */
@Path("SalaController")
public class SalaController {

    @EJB
    SalaService salaService;

    @POST
    @Path("/guardar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardar(SalaDto sala) {
        try {
            Respuesta res = salaService.guardar(sala);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((SalaDto) res.getResultado("Sala")).build();
        } catch (Exception e) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar la sala").build();
        }
    }

    @POST
    @Path("/actualizar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response actualizar(SalaDto sala) {
        try {
            Respuesta res = salaService.guardar(sala);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((SalaDto) res.getResultado("Sala")).build();
        } catch (Exception e) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar la sala").build();
        }
    }

    @POST
    @Path("/cargarAsientos/{idSala}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cargarAsientos(@PathParam("idSala") Long idSala, List<AsientoDto> asientos) {
        try {
            Respuesta res = salaService.cargarAsientos(idSala, asientos);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((SalaDto) res.getResultado("Sala")).build();
        } catch (Exception e) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error al guardar la sala").build();
        }
    }

    @GET
    @Path("/getSala/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getSala(@PathParam("id") Long id) {
        try {
            Respuesta res = salaService.getSala(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((SalaDto) res.getResultado("sala")).build();

        } catch (Exception ex) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las salas").build();
        }
    }

    @GET
    @Path("/getSalas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)//es bueno tener estas terminacionesd
    public Response getSalas() {
        try {
            Respuesta res = salaService.getSalas();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<SalaDto>>((List<SalaDto>) res.getResultado("Salas")) {
            }).build();//jocser
        } catch (Exception ex) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las salas").build();
        }
    }

    @DELETE
    @Path("/eliminarSalas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarSalas(List<SalaDto> salas) {
        try {
            Respuesta respuesta = salaService.eliminarSalas(salas);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();

        } catch (Exception ex) {
            Logger.getLogger(SalaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).encoding("Error al buscar las salas").build();
        }
    }

}
