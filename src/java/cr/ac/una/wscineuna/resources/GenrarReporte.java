/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.resources;

import cr.ac.una.wscineuna.model.ReporteDto;
import cr.ac.una.wscineuna.util.CodigoRespuesta;
import cr.ac.una.wscineuna.util.ConverArchivo;
import cr.ac.una.wscineuna.util.Respuesta;
import cr.ac.una.wscineuna.util.GenerarUrl;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class GenrarReporte {

    private String recuperar = GenerarUrl.direccionR_Reporte();
    private String guardar = GenerarUrl.direccionG_Reporte();
    private String arCode = "";

    public GenrarReporte() {
    }

    public Respuesta generarReportePrueba() {
        try {
            Integer us = new Integer(1);
//            Map<String, Object> parametros = new HashMap<>();
//            parametros.put("id", us);
            String cadenaConexion = "jdbc:oracle:thin:@localhost:1521:XE";
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection(cadenaConexion,
                    "cineuna", "cineuna");
            JasperReport jasperReport;
            URL in = this.getClass().getResource("report1.jasper");
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JasperPrint reporte = JasperFillManager.fillReport(jasperReport, null, con);

            JasperExportManager.exportReportToPdfFile(reporte, (guardar) + "prueba.pdf");

            arCode = ConverArchivo.fileToString(recuperar + "prueba.pdf");
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", new ReporteDto(arCode));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "El reporte no se pudo", "");
        }
    }

    public Respuesta generarReporteTandas(ReporteDto rep) {
        try {
            String cadenaConexion = "jdbc:oracle:thin:@localhost:1521:XE";
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection(cadenaConexion,
                    "cineuna", "cineuna");

            Map<String, Object> parametros = new HashMap<>();
            parametros.put("fec1", rep.getFecha1().toString());
            parametros.put("fec2", rep.getFecha2().toString());

            JasperReport jasperReport;
            URL in = this.getClass().getResource("Tanda.jasper");
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JasperPrint reporte = JasperFillManager.fillReport(jasperReport, parametros, con);

            JasperExportManager.exportReportToPdfFile(reporte, (guardar) + "Tanda.pdf");

            arCode = ConverArchivo.fileToString(recuperar + "Tanda.pdf");

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "","","Reporte", new ReporteDto(arCode));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "El reporte no se pudo", "");
        }
    }

    public Respuesta generarReportePelicualas(Long idPeli) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", idPeli);

            String cadenaConexion = "jdbc:oracle:thin:@localhost:1521:XE";
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection(cadenaConexion,
                    "cineuna", "cineuna");

            JasperReport jasperReport;
            URL in = this.getClass().getResource("Peliculas.jasper");
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JasperPrint reporte = JasperFillManager.fillReport(jasperReport, parametros, con);

            JasperExportManager.exportReportToPdfFile(reporte, (guardar) + "pelicula.pdf");

            arCode = ConverArchivo.fileToString(recuperar + "pelicula.pdf");
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", new ReporteDto(arCode));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "El reporte no se pudo", "");
        }
    }

    public Respuesta generarReporteCompra(Long idCompra, String correo, String msj, String nombreArchivo) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", idCompra);

            String cadenaConexion = "jdbc:oracle:thin:@localhost:1521:XE";
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection(cadenaConexion,
                    "cineuna", "cineuna");

            JasperReport jasperReport;
            URL in = this.getClass().getResource("Compra2.jasper");
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JasperPrint reporte = JasperFillManager.fillReport(jasperReport, parametros, con);

            JasperExportManager.exportReportToPdfFile(reporte, (guardar) + "Compra2.pdf");
//
            arCode = ConverArchivo.fileToString(recuperar + "Compra2.pdf");
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Comprobante", new ReporteDto(arCode));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "El comprobante no se pudo enviar", "");
        }
    }
}
