/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.util;

import cr.ac.una.wscineuna.controller.PeliculaController;

/**
 *
 * @author Matami
 */
public class GenerarUrl {

    private GenerarUrl() {
    }
/**
 * Genera la direccion de recuperado para las imagenes
 * @return 
 */
    public static String direccionR() {

        String dre = PeliculaController.class.getSimpleName() + ".class";
        String rutaca = PeliculaController.class.getResource(dre).toString();
        rutaca = rutaca.replace("/C", "///C");
        rutaca = rutaca.replace("/build", "/Imagenes");//Imagenes
        rutaca = rutaca.replace("/web", "");
        rutaca = rutaca.replace("/WEB-INF", "");
        rutaca = rutaca.replace("/classes", "");
        rutaca = rutaca.replace("/controller", "");
        rutaca = rutaca.replace("/PeliculaController.class", "");
        rutaca = rutaca.replace("/cr", "");
        rutaca = rutaca.replace("/ac", "");
        rutaca = rutaca.replace("/wscineuna", "/");
//        rutaca = rutaca.replace(" ", "%");
        return rutaca;
    }
/**
 * genera la direccion de guardado para las imagenes
 * @return 
 */
    public static String direccionG() {

        String dre = PeliculaController.class.getSimpleName() + ".class";
        String rutaca = PeliculaController.class.getResource(dre).toString();
        rutaca = rutaca.replace("/", "\\\\");
        rutaca = rutaca.replace("file:", "");
        rutaca = rutaca.replace("\\\\C", "C");
        rutaca = rutaca.replace("\\\\build", "\\\\Imagenes");//Imagenes
        rutaca = rutaca.replace("\\\\web", "");
        rutaca = rutaca.replace("\\\\WEB-INF", "");
        rutaca = rutaca.replace("\\\\classes", "");
        rutaca = rutaca.replace("\\\\controller", "");
        rutaca = rutaca.replace("\\\\PeliculaController.class", "");
        rutaca = rutaca.replace("\\\\cr", "");
        rutaca = rutaca.replace("\\\\ac", "");
        rutaca = rutaca.replace("\\\\wscineuna", "\\\\");
        rutaca = rutaca.replace("%20", " ");
        return rutaca;
    }
/**
 * genera la direccion de guardado para los reportes
 * @return 
 */
    public static String direccionG_Reporte() {

        String dre = PeliculaController.class.getSimpleName() + ".class";
        String rutaca = PeliculaController.class.getResource(dre).toString();
        rutaca = rutaca.replace("/", "\\\\");
        rutaca = rutaca.replace("file:", "");
        rutaca = rutaca.replace("\\\\C", "C");
        rutaca = rutaca.replace("\\\\build", "\\\\Reportes");
        rutaca = rutaca.replace("\\\\web", "");
        rutaca = rutaca.replace("\\\\WEB-INF", "");
        rutaca = rutaca.replace("\\\\classes", "");
        rutaca = rutaca.replace("\\\\controller", "");
        rutaca = rutaca.replace("\\\\PeliculaController.class", "");
        rutaca = rutaca.replace("\\\\cr", "");
        rutaca = rutaca.replace("\\\\ac", "");
        rutaca = rutaca.replace("\\\\wscineuna", "\\\\");
        rutaca = rutaca.replace("%20", " ");
        return rutaca;
    }
    /**
     * genera la direccion de recueperado para los reportes
     * @return 
     */
    public static String direccionR_Reporte() {

  
        String dre = PeliculaController.class.getSimpleName() + ".class";
        String rutaca = PeliculaController.class.getResource(dre).toString();
        rutaca = rutaca.replace("/C", "///C");
        rutaca = rutaca.replace("/build", "/Reportes");//Imagenes
        rutaca = rutaca.replace("/web", "");
        rutaca = rutaca.replace("/WEB-INF", "");
        rutaca = rutaca.replace("/classes", "");
        rutaca = rutaca.replace("/controller", "");
        rutaca = rutaca.replace("/PeliculaController.class", "");
        rutaca = rutaca.replace("/cr", "");
        rutaca = rutaca.replace("/ac", "");
        rutaca = rutaca.replace("/wscineuna", "/");
//        rutaca = rutaca.replace(" ", "%");
        return rutaca;
    }

}
