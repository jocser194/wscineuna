/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wscineuna.util;

/**
 *
 * @author Matami
 */
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;

public class ConverArchivo {

    /**
     * Recibe el string de la imagen y lo pasa a archivo Image
     *
     * @param imageString String de la imagen
     * @return retorna el archivo image
     */
    public static Image stringToImage(String imageString) {
        Image im;

        BufferedImage image = null;
        byte[] imageByte;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return im = SwingFXUtils.toFXImage(image, null);
    }
 

    /**
     * Pasa de el string de la imagen al bufer de la misma
     *
     * @param imageString
     * @return
     */
    public static BufferedImage stringToBufferedImage(String imageString) {

        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);;
        byte[] imageByte;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Pasa de imagen a String
     *
     * @param im recibe una Image
     * @param tipoImage Tipo de imagen
     * @return El string de la imagen
     */
    public static String imageToString(Image im, String tipoImage) {

        BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
        //pasa de imagen a buffer o al reves
        image = SwingFXUtils.fromFXImage(im, null);

        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, tipoImage, bos);
            byte[] imageBytes = bos.toByteArray();

            Base64.Encoder encode = Base64.getEncoder();
            imageString = encode.encodeToString(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    /**
     * codifica el archivo de la imagen en un string para enviarlo por la red
     *
     * @param im recibe la direccion del archvio o imagen
     * @return
     */
    public static String fileToString(String im) throws URISyntaxException {
        Base64.Encoder encode = Base64.getEncoder();
// 
        try {

            File file = new File(new URI(im));
            byte[] fileArray = new byte[(int) file.length()];
            String encodedFile = "";
            InputStream inputStream;
            inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            encodedFile = encode.encodeToString(fileArray);
            return encodedFile;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void elimiarFile(String im) throws URISyntaxException {
        Base64.Encoder encode = Base64.getEncoder();
// 
        try {

            File file = new File(new URI(im));
            file.delete();
        } catch (Exception e) {
            System.out.println(e.getMessage());
//            return null;
        }
    }
    
       /**
     * Crea el reporte en la carpeta reportes
     * @param arCode
     * @return 
     */
    public static String stringToFile(String arCode) {

        try {
            byte[] imageByte;
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(arCode);

            LocalDate fecha = LocalDate.now();
            OutputStream out = new FileOutputStream("Reportes/" + fecha.toString());
            out.write(imageByte);
            out.close();
            return "Reportes/" + fecha.toString();
        } catch (Exception n) {
            System.out.println(n.getMessage());
            return null;
        }
    }
}
